#!/usr/bin/env python3

from os.path import splitext
import cv2 as cv
import numpy as np

def warp_it_yo(img, rotation_degrees=0, tx=0, ty=0):
    " Corrupt the image by rotating and translating it "
    rotation_rads = np.deg2rad(rotation_degrees)
    s, c = np.sin(rotation_rads), np.cos(rotation_rads)
    M = np.float64([[c, -s, tx],[s, c, ty]])
    s = img.shape
    return cv.warpAffine(img, M, (s[1], s[0]), flags=cv.INTER_LINEAR + cv.WARP_INVERSE_MAP);


def align_ecc(fixed, moving, motion, iterations=5000, epsilon=1e-10):
    " Align the moving image to the fixed image by finding the ECC transform "
    assert motion in (cv.MOTION_TRANSLATION, cv.MOTION_AFFINE)

    # Convert images to grayscale for faster processing
    fixed_g = cv.cvtColor(fixed, cv.COLOR_BGR2GRAY)
    moving_g = cv.cvtColor(moving, cv.COLOR_BGR2GRAY)

    # Specify ECC algorithm parameters and find the transform
    criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, iterations,  epsilon)
    _, M = cv.findTransformECC(fixed_g, moving_g, np.eye(2, 3, dtype=np.float32), motion, criteria)

    # Return the aligned image
    s = tuple(reversed(fixed.shape[:2]))
    return cv.warpAffine(moving, M, s, flags=cv.INTER_LINEAR + cv.WARP_INVERSE_MAP)


def main(imagefile1, imagefile2, motion):
    assert motion in ("translation", "affine")

    img1 = cv.imread(imagefile1)
    img2 = cv.imread(imagefile2)
    # Warp the moving image so the algorithm has some work to do
    #img2 = warp_it_yo(img2, 10, 30, 50)
    #cv.imwrite("flower_warped.jpg", img2)

    # Align the image using ECC
    print("Computing ECC alignment using {} transformation...".format(motion))
    motion = cv.MOTION_AFFINE if motion == "affine" else cv.MOTION_TRANSLATION
    img2_aligned = align_ecc(img1, img2, motion)

    # Write the aligned image to file
    outfile = "{}_aligned{}".format(*splitext(imagefile2))
    print("Writing {!r}".format(outfile))
    cv.imwrite(outfile, img2_aligned)


if __name__ == "__main__":
    import sys
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("imagefile1")
    ap.add_argument("imagefile2")
    ap.add_argument("--motion", choices=("affine", "translation"), default="affine")
    sys.exit(main(**vars(ap.parse_args())))
