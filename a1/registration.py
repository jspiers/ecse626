from imageutils import mutual_information, mean_square_diff, yield_translations

def brute_force_similarity(I_moving, I_fixed, similarity_func, max_iterations):
    " Brute force search for best translation "
    # initialize maxima
    max_similarity, max_position, max_transformed = None, None, None

    # brute force search of best translation
    last_change_iter = 0
    for i, (I_moved, x, y) in enumerate(yield_translations(I_moving), 1):
        sim = similarity_func(I_moved, I_fixed)
        if max_similarity is None or sim > max_similarity:
            max_similarity, max_position, max_transformed = sim, (x,y), I_moved
            print(max_position, max_similarity)
            last_change_iter = i
        else:
            no_improvement = i - last_change_iter
            if no_improvement == max_iterations:
                print("That's enough - stopping after no improvement in {} iterations".format(max_iterations))
                break
            if not no_improvement % (max_iterations // 10):
                print("No improvement in last {} iterations...".format(no_improvement))

    return max_position, max_similarity, max_transformed

def image_registration_mi(I_moving, I_fixed, max_iter):
    print("Brute force method")
    best_translation, mi, I_moved = brute_force_similarity(I_moving, I_fixed, mutual_information, max_iter)
    print("Best translation {} has MI = {:6.4f}".format(best_translation, mi))
    return I_moved

def least_square_diff(*args, **kwargs):
    return -mean_square_diff(*args, **kwargs)

def image_registration_msd(I_moving, I_fixed):
    print("Brute force method")
    best_translation, lsd, I_moved = brute_force_similarity(I_moving, I_fixed, least_square_diff, 4*I_moving.size)
    print("Best translation {} has MSD = {:6.4f}".format(best_translation, -lsd))
    return I_moved
