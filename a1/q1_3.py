#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import cv2 as cv
import numpy as np
from matplotlib import cm, pyplot as plt
from IPython.display import display, Markdown

from imageutils import mutual_information, mean_square_diff,                        translate, yield_translations
from registration import image_registration_mi, image_registration_msd

# For reproducible results
np.random.seed(0)


# In[2]:


def load_images(prefix):
    filenames = ["I{}_{}.png".format(prefix, n) for n in (1, 2)]
    for fn in filenames:
        assert os.path.isfile(fn), fn + " is not a file"
    return [cv.imread(fn, cv.IMREAD_GRAYSCALE) for fn in filenames]

# Load brain images
I1_1, I1_2 = load_images(1)
I2_1, I2_2 = load_images(2)


# In[3]:


# Plot images
def plot_image_pairs(imgs, scale=6):    
    cols = 2
    rows = (len(imgs) + 1) // 2
    width = cols * scale
    height = rows * scale
    _ = plt.figure(figsize=(width, height))
    for i, img in enumerate(imgs, 1):
        _ = plt.subplot(rows, cols, i)
        _ = plt.imshow(img, cmap=cm.gray)
    _ = plt.show()


# # Images I1_1 and I1_2

# In[4]:


print("I1_1 and I1_2")
# plot_image_pairs([I1_1, I1_2], 8)


# ## Mutual Information

# In[5]:


print("Registering by maximizing mutual information")
I1_mi = image_registration_mi(I1_1, I1_2, 2000)


# ## Mean Square Difference

# In[6]:


print("Registering by minimizing mean square difference")
I1_msd = image_registration_msd(I1_1, I1_2)


# # Images I2_1 and I2_2

# In[7]:


print("I2_1 and I2_2")
# plot_image_pairs([I2_1, I2_2], 8)


# ## Mutual Information

# In[8]:


print("Registering by maximizing mutual information")
I2_mi = image_registration_mi(I2_1, I2_2, 2000)


# ## Mean Square Difference

# In[9]:


print("Registering by minimizing mean square difference")
I2_msd = image_registration_msd(I2_1, I2_2)


# # Results

# In[13]:


def overlay(img1, img2):
    return (img1 + img2) / 2

# Compare performance of MI and MSD based transformations by
# overlaying the moved image and the fixed image
_ = plt.figure(figsize=(12,6))
_ = plt.subplot(121)
_ = plt.imshow(overlay(I1_mi, I1_2), cmap=cm.gray)
_ = plt.title("I1_1 Registered to I1_2 using MI")
_ = plt.subplot(122)
_ = plt.imshow(overlay(I1_msd, I1_2), cmap=cm.gray)
_ = plt.title("I1_1 Registered to I1_2 using MSD")
_ = plt.show()

_ = plt.figure(figsize=(12,6))
_ = plt.subplot(121)
_ = plt.imshow(overlay(I2_mi, I2_2), cmap=cm.gray)
_ = plt.title("I2_1 Registered to I2_2 using MI")
_ = plt.subplot(122)
_ = plt.imshow(overlay(I2_msd, I2_2), cmap=cm.gray)
_ = plt.title("I2_1 Registered to I2_2 using MSD")              
_ = plt.show()


# # Discussion
# 
# For the first pair of images (I1_1, I1_2), registration by maximizing mutual information produces a slightly better alignment than registration by minimizing mean square difference.  In contrast, for the second pair of images (I2_1, I2_2), registration by maximizing mutual information produces a significantly worse alignment than registration by minimizing mean square difference.
# 
# ## Explanation: consistency of pixel values versus consistent mapping of pixel values
# The inferior performance of the MSD-based registration method for the first image pair results from the fact that MSD is not robust to variations in pixel intensities of the same features between images.  In other words, because some regions of the brain in the first moving image are lighter or darker than those same regions of the brain in the first fixed image, the MSD of the two images will remain high even when those features are aligned.
# 
# On the other hand, the MI metric does not require proximity in pixel _values_ of features between images as long as there is a relatively consistent _mapping_ of pixel values of features from the moving image to pixel intensities of those same features in the fixed image.  In other words, knowing the approximate pixel intensity of a brain feature in the moving image provides a relatively reliable prediction of the pixel values to be expected for that brain feature in the fixed image.  This is the type of registration task where mutual information is the right metric.
# 
# ## Limitations of mutual information
# In the second image pair, however, the mapping of pixel intensities of brain features from the moving image to the fixed image is much less consistent.  Mutual information is maximized when the joint entropy of pixel intensities of features across images is low; when pixel intensities of features in one image do not provide reliable information about the pixel intensities of features in the other image, the joint entropy will be high even when those features come into alignment.
# 
# ## Summary
# Mean square difference is a good metric for registration when there are consistent __values__ of pixel intensities between the same features in the two images. Mutual information is a good metric for registration when there is a consistent __mapping__ of pixel intensities between the same features in the two images, .
# 

# In[ ]:




