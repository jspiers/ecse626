import os
import sys
import unittest
import cv2

sys.path.insert(0, os.path.dirname(__file__))
from imageutils import pdf, entropy

class Entropy(unittest.TestCase):
    def load_image(self):
        return cv2.imread(os.path.join(os.path.dirname(__file__), "img_retina.jpg"))

    def test_entropy(self):
        img = self.load_image()
        h = entropy(pdf(img))
        self.assertGreater(h, 6.847)
        self.assertLess(h, 6.848)

    @unittest.expectedFailure
    def test_fail(self):
        self.fail()

if __name__ == "__main__":
    unittest.main()
