def local_search_similarity(I_moving, I_fixed, similarity_func, initial_translation, max_iterations=500):
    " Starting with the initial translation, adjust x and y so long as it increases the similarity "
    tx, ty = initial_translation
    I_moved = translate(I_moving, tx, ty)
    sim = similarity_func(I_moved, I_fixed)
    for i in range(max_iterations):
        for (x, y) in ((tx+1, ty), (tx, ty+1), (tx-1, ty), (tx, ty-1)):
            I_moved_new = translate(I_moving, x, y)
            sim_new = similarity_func(I_moved_new, I_fixed)
            if sim_new > sim:
                #print((x,y), sim_new)
                tx, ty = x, y
                I_moved = I_moved_new
                sim = sim_new
                break
        else:
            print("local maximum of {:7.5f} at {} after {} iterations".format(sim, (tx,ty), i))
            break
    else:
        print("stopped after max of {} iterations".format(i))
    return (tx, ty), sim, I_moved

def seeded_local_search(I_moving, I_fixed, similarity_func):
    best_translation, max_similarity, best_I_moved = None, None, None
    h, w = I_moving.shape[:2]
    xstep = w // 4
    ystep = h // 4
    for x in range(-w+xstep, w+1-xstep, xstep):
        for y in range(-h+ystep, h+1-ystep, ystep):
            print("Starting from {}...".format((x,y)), end=' ')
            trans, sim, I_moved = local_search_similarity(I_moving, I_fixed, similarity_func, (x,y))
            if max_similarity is None or sim > max_similarity:
                best_translation, max_similarity, best_I_moved = trans, sim, I_moved
    return best_translation, max_similarity, best_I_moved
