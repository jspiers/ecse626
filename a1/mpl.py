from matplotlib import cm, pyplot as plt

def plot_images(imgs, cols=1, scale=6, labels=()):
    rows = (len(imgs) + 1) // 2
    width = cols * scale
    height = rows * scale
    _ = plt.figure(figsize=(width, height))
    for i, img in enumerate(imgs):
        _ = plt.subplot(rows, cols, i+1)
        _ = plt.imshow(img, cmap=cm.gray)
        if labels:
            _ = plt.title(labels[i])
    _ = plt.show()
