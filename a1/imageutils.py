import math
import json
from itertools import product
import numpy as np
import cv2 as cv
from scipy.signal import parzen

def rgbgr(cv_img):
    " Swap red and blue color channels (i.e. convert an OpenCV BGR image to/from Matplotlib RGB) "
    if len(cv_img.shape) > 2:
        return cv.cvtColor(cv_img, cv.COLOR_BGR2RGB)
    return cv_img

def resize(img, scale=None, width=None, height=None):
    h, w = img.shape[:2]
    num_args = sum(1 if a is not None else 0 for a in (width, height, scale))
    assert num_args == 1, "resize() takes one and only one of 'scale', 'width', 'height' (got {})".format(num_args)
    scale = scale if scale is not None else width/w if width is not None else height/h
    if scale == 1.0:
        return img
    interp = cv.INTER_LANCZOS4 if scale > 1 else cv.INTER_AREA
    return cv.resize(img, None, fx=scale, fy=scale, interpolation=interp)

def discrete_uniform_noise(img, level):
    " Add discrete uniform noise in the range [-level, level] "
    return np.random.randint(-level, level+1, img.shape)

def pdf(*imgs):
    " Calculates the joint probability distribution of pixel values over all the images "
    bins = [img.max() + 1 - img.min() for img in imgs]
    hist, bins = np.histogramdd([img.ravel() for img in imgs], bins=bins)
    return hist / hist.sum(), [(int(b.min()), int(b.max())) for b in bins]

def entropy(pdf):
    " Calculates the entropy of the distribution "
    # Filter out zero probability values
    nonzero = pdf[0][pdf[0]!=0]
    return -(np.log2(nonzero) * nonzero).sum()

def mutual_information(img1, img2):
    " Calculate the mutual information between the images from the independent and joint entropies "
    return entropy(pdf(img1)) + entropy(pdf(img2)) - entropy(pdf(img1, img2))

def mean_square_diff(img1, img2):
    " Calculate the mean of squared differences between the two images "
    assert img1.shape == img2.shape
    diff = img1 - img2
    return (diff * diff).mean()

def align_ranges(pdfA, pdfB):
    " Pad the PDFs with zeros, if necessary, to make sure they share the same ranges "
    p_A, ranges_A = pdfA
    p_B, ranges_B = pdfB

    # Note: currently only implemented for one-dimensional PDFs
    assert len(ranges_A) == len(ranges_B) == 1, (len(ranges_A), len(ranges_B))
    min_A, max_A = ranges_A[0]
    min_B, max_B = ranges_B[0]
    
    # Pad the lower end of the range
    if min_A < min_B:
        p_B = np.concatenate((np.zeros(min_B-min_A), p_B))
    elif min_A > min_B:
        p_A = np.concatenate((np.zeros(min_A-min_B), p_A))

    # Pad the upper end of the range    
    if max_A < max_B:
        p_A = np.concatenate((p_A, np.zeros(max_B-max_A)))
    elif max_A > max_B:
        p_B = np.concatenate((p_B, np.zeros(max_A-max_B)))

    ranges = [(min(min_A, min_B), max(max_A, max_B))]
    
    # Sanity check
    assert len(p_A) == len(p_B) == ranges[0][1] - ranges[0][0] + 1

    return (p_A, ranges), (p_B, ranges)

def kl_divergence(pdfA, pdfB):
    " Calculate the Kullback-Leibler divergence between the images A and B "
    pdfA_aligned, pdfB_aligned = align_ranges(pdfA, pdfB)
    zipped = list(zip(np.nditer(pdfA_aligned[0]), np.nditer(pdfB_aligned[0])))
    if any(p and not q for p, q in zipped):
        return np.Infinity
    return sum((p * math.log2(p/q)) for p, q in zipped if p)

def parzen_filter(pdf, n):
    " Apply a Parzen filter by convolving a Parzen window with the pdf and normalizing "
    assert n % 2, "parzen_filter only implemented for odd window size (got {})".format(n)
    window = parzen(n)
    convolved = np.convolve(pdf[0], window, mode="same")
    return convolved / convolved.sum(), pdf[1]

def translate(img, x, y):
    " Return a copy of img translated by (x, y) pixels "
    assert len(img.shape) == 2, "grayscale only"
    rows, cols = img.shape
    assert abs(x) < cols, "x-translation of {} exceeds image bounds {}x{}".format(x, *reversed(img.shape))
    assert abs(y) < rows, "y-translation of {} exceeds image bounds {}x{}".format(y, *reversed(img.shape))
    M = np.float32([[1, 0, x], [0, 1, y]])
    return cv.warpAffine(img, M, (cols, rows))

def yield_translations(img, step=1):
    " Yield (x, y) translated images in order from smallest to largest translations "
    h, w = img.shape[:2]
    all_translations = list(product(range(-w+1,w,step), range(-h+1,h, step)))
    for x, y in sorted(all_translations, key=lambda xy: xy[0]*xy[0] + xy[1]*xy[1]):
        yield translate(img, x, y), x, y

class NumpyEncoder(json.JSONEncoder):
    " JSON encoder class for numpy ndarrays (https://stackoverflow.com/a/47626762) "
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
    
# def snapshot_PCA(M):
#     # Calculate the mean image
#     u = M.mean(1)
    
#     # Subtract the mean image from each image
#     D = (M.transpose() - u).transpose()

#     # Eigendecomposition of the Compute the eigenvalues and eigenvectors
#     evalues, evectors = np.linalg.eig(D@D.T)
#     return u, evalues, evectors
# u, evalues, evectors = snapshot_PCA(X)
# evalues.shape, evectors.shape

# _ = plt.figure()
# _ = plt.imshow(u.reshape(shape), cmap=cm.gray)
# _ = plt.show()