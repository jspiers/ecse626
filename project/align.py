#!/usr/bin/env python3

# Standard Python
import sys
import os
import logging
import pickle
from itertools import groupby, count, cycle
from operator import itemgetter
from pprint import pprint
from collections import defaultdict

# Non-Standard Libraries
import numpy as np
import skimage
from skimage.io import imsave
from skimage.util import img_as_ubyte
from skimage.transform import AffineTransform, warp, resize
from skimage.filters import sobel
from mnist import MNIST

# Debug
# from pdb import set_trace
# import gnureadline as readline

# Local imports
from transform import Transform

# For reproducibility
np.random.seed(0)

# Initialize gzipped MNIST image data located in MNIST_DIR directory
MNIST_DIR = "mnist"
MNIST_SHAPE = (28, 28)

def load_mnist(train_test):
    assert train_test in ("train", "test")

    # Create MNIST loader object and tell it to use gzip versions of files in MNIST_DIR
    mndata = MNIST(MNIST_DIR)
    mndata.gz = True

    # Load images (as 1D-lists of [0, 255] intensity values)
    if train_test == "train":
        images, labels = mndata.load_training()
    else: # train_test == "test":
        images, labels = mndata.load_testing()

    # Group images by label and convert images
    # from lists of pixel values to uint8 numpy arrays
    for label, label_images in groupby(sorted(zip(labels, images)), itemgetter(0)):
        images = [np.array(li[1], dtype=np.uint8).reshape(*MNIST_SHAPE)
                  for li in label_images]
        yield label, images


def binary_threshold(images, threshold=None):
    " Threshold pixel intensity values >= threshold to max and < threshold to min dtype value "
    type_info = np.iinfo(images.dtype)
    min_, max_ = type_info.min, type_info.max
    if threshold is None:
        threshold = (min_ + max_) / 2
    return np.where(images >= threshold, max_, min_).astype(images.dtype)


def _check_padding(images, minimum):
    # DEBUG: Get min, max indices of non-zero values
    _, nonzero_rows, nonzero_cols = images.nonzero()
    row_min, row_max = (nonzero_rows.min(), nonzero_rows.max())
    col_min, col_max = (nonzero_cols.min(), nonzero_cols.max())
    logging.debug("Non-zero indices:")
    logging.debug("\trows %d %d", row_min, row_max)
    logging.debug("\tcols %d %d", col_min, col_max)
    min_padding = min(row_min, col_min, images.shape[-1]-1-row_max, images.shape[-1]-1-col_max)
    logging.debug("\t... therefore zero-padding of %d", min_padding)
    return min_padding >= minimum


class Congealer:

    BETA = 0.001

    def __init__(self, images, beta=None):
        self._beta = beta if beta is not None else self.BETA

        # Check stack shape
        assert len(images.shape) == 3, "Expected 3D image stack, got shape {}".format(images.shape)
        self._shape = images.shape

        # Reduce pixel-value range by projecting images into a space
        # of indices in an array of intensity values
        logging.debug("images %s %r", images.dtype, images.max())
        self._intensities, self._indices = np.unique(images, return_inverse=True)
        print("{} unique intensity values:".format(self._intensities.size))
        print(self._intensities)
        self._indices = self._indices.reshape(self._shape).astype(np.uint8)
        logging.debug("indices %s %r", self._indices.dtype, self._indices.max())
        self._original = self._indices.copy()

        # Select (much faster) binary stack entropy cost function when possible
        binary = len(self._intensities) == 2
        self._entropy = self._stack_entropy_binary if binary else self._stack_entropy

        # Initialize transforms to identity (no effect)
        self._transforms = [Transform() for _ in images]

#     @staticmethod
#     def stack_entropy_both(images):
#         " Verify consistency of binary and general stack entropy functions "
#         h_binary = stack_entropy_binary(images)
#         h_general = stack_entropy(images)
#         assert np.abs(h_binary - h_general) < 1e-10, (h_binary, h_general)
#         return h_binary

    def _stack_entropy_binary(self):
        """ Shortcut for binary case:
                p(1) in a [0, 1] distribution = E[distribution]
                p(0) = 1 - E[distribution]
        """
        p_ones = self._indices.mean(axis=0)
        p = np.hstack((p_ones, 1 - p_ones))
        return -np.where(p != 0, p * np.log2(p), 0).sum()

    def _stack_entropy(self):
        " Compute sum of pixel-stack entropies "
        # Calculate distribution over each pixel stack using bincount
        pstacks = self._indices.reshape(self._shape[0], -1).T
        bincounts = tuple(np.bincount(x) for x in pstacks)
        p = np.hstack(bincounts) / self._shape[0]

        # Filter out zeros and calculate sum of entropies across all stacks
        return -np.where(p != 0, p * np.log2(p), 0).sum()

    def _penalty(self):
        return self._beta * sum(t.penalty() for t in self._transforms)

    def _compute_cost(self):
        self.penalty = self._penalty()
        self.cost = self._entropy() + self._penalty()

    def _subtract_mean(self):
        """ Subtract mean transform from all transforms
            and apply normalized transforms to original images
        """
        mean_transform = Transform.mean(self._transforms)
        print("\nSubtracting mean transform: {}".format(mean_transform))
        self._transforms = [t - mean_transform for t in self._transforms]
        retransformed = [t.apply(orig) for (t, orig) in zip(self._transforms, self._original)]
        self._indices = np.stack(retransformed, axis=0)
        self._compute_cost()
        print("{:.2f} {:.3f}".format(self.cost, self.penalty))

    def _try_transform(self, transform, j):
        " Try a new transform for image j and keep it if it reduces the cost "
        # Apply transform to original image
        transformed = transform.apply(self._original[j])

        # Replace current image in the stack with new transformed image
        # (while keeping track of the previous image)
        prev_img = self._indices[j, ...].copy()
        self._indices[j, ...] = transformed

        # Compute penalty term
        marginal_penalty = self._beta * (transform.penalty() - self._transforms[j].penalty())
        penalty = self.penalty + marginal_penalty

        # Compute new cost
        cost = self._entropy() + penalty

        # If entropy + penalty is lower, accept the transform and the new image stack
        if cost < self.cost:
            # Accept new transform (and its associated cost and penalty)
            self._transforms[j] = transform
            self.cost = cost
            self.penalty = penalty
            return True
        else:
            # Revert image stack
            self._indices[j, ...] = prev_img
            return False

    def align(self, subtract_mean="never", vanilla=False):
        " Align stack of images by minimizing sum of pixel-stack entropies "
        print("Aligning {} {}x{} images...".format(*self._shape))

        # Compute initial cost, entropy and penalty
        self._compute_cost()
        assert 0 <= self.penalty < 1e-10
        print("Initial entropy = {:.2f} bits".format(self.cost))

        # Loop until convergence
        self._n_transforms = 0
        stats = {}
        unchanged_count = 0
        mean_subtracted = False # for "end" mean subtraction mode
        for j, orig in cycle(enumerate(self._original)):

            # Check for loop termination (no changes)
            if unchanged_count == self._shape[0]:
                if subtract_mean != "end" or mean_subtracted:
                    print('')
                    break

                # Perform mean subtraction at "end":
                #   i.e. when cost function has settled and we would otherwise exit the loop.
                # Set "mean_subtracted" to True and see if we can get through
                #   another complete loop over all images without it getting set False again,
                #   which happens when any image transform is updated
                self._subtract_mean()
                mean_subtracted = True
                unchanged_count = 0

            # Indicator for each image
            print(".", end='')
            sys.stdout.flush()

            # Compute groups of marginal transforms to be tried
            # - In the vanilla Learned-Miller approach there are k groups, 1 for each transformation
            # parameter, with parameter k being adjusted by +/- delta_k
            # - In the modified approach, we have a single group of 10 marginal transforms selected
            # from a normal distribution with standard deviation of 1 delta for each transformation
            # parameter

            # Keep track of whether any change to the transformation parameters
            # for this image was accepted
            changed = False
            k_range = len(Transform.DELTAS) if vanilla else 1
            for k in range(k_range):
                marginal_transforms = self._transforms[j].yieldMarginals(k) if vanilla else (self._transforms[j].marginalGaussian(1) for _ in range(10))
                for transform in marginal_transforms:

                    # Update total number of transformations
                    self._n_transforms += 1

                    transform_diff = transform - self._transforms[j]

                    # Try applying transform to original image, accept if cost is lowered
                    if self._try_transform(transform, j):
                        # Report
                        print("\n{} {:.2f} {:.3f} {:3} {}".format(self._n_transforms,
                                                                  self.cost,
                                                                  self.penalty,
                                                                  j,
                                                                  transform_diff))
                        changed = True
                        mean_subtracted = False
                        stats[self._n_transforms] = self.cost
                        break

            # Reset or increment unchanged counter depending whether a new transformation was accepted
            unchanged_count = 0 if changed else unchanged_count + 1

            # Subtract mean transform from each transform such that mean transform is identity
            if subtract_mean == "every" and j == self._shape[0]-1:
                self._subtract_mean()

        return stats

    def aligned_images(self):
        return self._intensities[self._indices]

    def transforms(self):
        return self._transforms

def mean_square_gradient(img):
    " Compute mean square gradient of the image, a measure of sharpness "
    gradient = sobel(img)
    return (gradient ** 2).mean()


def corrupt_images(images, std_dev):
    " Apply a random transform to each image in the images stack "
    return np.stack([Transform.gaussian(std_dev).apply(img) for img in images], axis=0)


def write_results(images, aligned_images, transforms, stats, output_dir, label):
    # Compute dictionary of non-identity transforms
    nonidentity = dict((i, str(t)) for (i, t) in enumerate(transforms) if t)
    print("Transforms:")
    pprint(nonidentity)

    # Compute mean transform and its deviation from the identity transform
    trans_mean = Transform.mean(transforms)
    print("Mean transform", trans_mean)
    trans_min = Transform.min(transforms)
    print("Min transform parameters", trans_min)
    trans_max = Transform.max(transforms)
    print("Max transform parameters", trans_max)

    # Write out before and after average images and compute their mean-square
    # gradients as a measure of alignment (higher is better)
    for before_after, stack in (("before", images), ("after", aligned_images)):
        avg = np.average(stack, axis=0).astype(np.uint8)
        image_filename = os.path.join(output_dir, "{}-{}.png".format(label, before_after))
        print("Writing {!r}".format(image_filename))
        imsave(image_filename, avg)
        msg = mean_square_gradient(avg)
        print("Mean square gradient {}: {:.6f}".format(before_after, msg))

    # Pickle the list of transforms
    pickle_filename = os.path.join(output_dir, "{}-transforms.pickle".format(label))
    print("Writing transforms for {!r} to {!r}".format(label, pickle_filename))
    with open(pickle_filename, 'wb') as f:
        pickle.dump(transforms, f)

    # Pickle the stats
    pickle_filename = os.path.join(output_dir, "{}-stats.pickle".format(label))
    print("Writing stats for {!r} to {!r}".format(label, pickle_filename))
    with open(pickle_filename, 'wb') as f:
        pickle.dump(stats, f)


def main(max_images, beta, corrupt, gray, subtract_mean, debug, digits, vanilla):

    # Activate debug messages if debug flag was set on the command line
    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)

    output_dir = "{}-m{}-{}-c{}-{}-b{}".format("vanilla" if vanilla else "gaussian",
                                               max_images,
                                               "gray" if gray else "bin",
                                               corrupt,
                                               subtract_mean,
                                               "{:f}".format(beta).replace('.', '_').rstrip('0'))

    print("Using output directory {!r}".format(output_dir))
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    print("Loading MNIST test images and grouping by digit...")
    for digit, images in load_mnist("test"):
        if digit not in digits:
            continue

        print("Digit: {}".format(digit))

        if max_images is not None and len(images) > max_images:
            print("Reducing image stack from {} to {} images".format(len(images), max_images))
            images = images[:max_images]

        # Resize
#         upshape = (140, 140)
#         print("Resizing images to {}".format(upshape))
#         images = [resize(img, upshape) for img in images]

        # Stack images into a single matrix
        images = np.stack(images, axis=0)

        if corrupt:
            print("Corrupting images with random transforms (standard dev {})...".format(corrupt))
            images = corrupt_images(images, corrupt)

        if not gray:
            print("Binary thresholding image intensities...")
            images = binary_threshold(images)

        congealer = Congealer(images, beta)

        stats = congealer.align(subtract_mean, vanilla)
        aligned_images = congealer.aligned_images()
        transforms = congealer.transforms()

        assert (images != aligned_images).any(), "No change"

        # Report results to stdout and JSON
        write_results(images, aligned_images, transforms, stats, output_dir, str(digit))


if __name__ == "__main__":
    import argparse
    print("Libraries:")
    print("\tPython v{}".format(sys.version.split(' ', 1)[0]))
    print("\tNumPy v{}".format(np.__version__))
    print("\tscikit-image v{}".format(skimage.__version__))

    ap = argparse.ArgumentParser("Continuous Joint Image Alignment")
    ap.add_argument("--beta", "-b", type=float, default=Congealer.BETA)
    ap.add_argument("--max_images", "-m", type=int, default=None)
    ap.add_argument("--debug", "-d", action="store_true")
    ap.add_argument("--gray", "-g", action="store_true")
    ap.add_argument("--corrupt", "-c", type=int, default=0)
    ap.add_argument("--subtract_mean", "-s", choices=["never", "every", "end"], default="never")
    ap.add_argument("--vanilla", "-v", action="store_true",
                    help="Vanilla Learned-Miller marginal transformations")
    ap.add_argument("digits", type=int, nargs='*', metavar="digit", default=tuple(range(10)))
    sys.exit(main(**vars(ap.parse_args())))
