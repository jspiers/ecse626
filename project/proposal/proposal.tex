% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

%\usepackage[
%doi=false
%,isbn=false
%,url=false
%,backend=biber
%,style=lncs
%]{biblatex}
\usepackage[backend=bibtex,style=lncs,natbib=true]{biblatex}


% FIXES FOR BROKEN lncs.bbx
\DeclareNameFormat{author}{%
	\ifdefvoid{\namepartprefix}{}{\namepartprefix\space}\namepartfamily, \namepartgiveni%
	\ifthenelse{\value{listcount}<\value{liststop}}
	{\addcomma\space}%
	{}%
}
\DeclareNameFormat{editor}{%
	\ifdefvoid{\namepartprefix}{}{\namepartprefix\space}\namepartfamily, \namepartgiveni%
	\ifthenelse{\value{listcount}<\value{liststop}}
	{\addcomma\space}%
	{\space\ifthenelse{\value{listcount}>1}
		{(\bibstring{editors})}
		{(\bibstring{editor})}}%
}
\renewcommand*{\finentrypunct}{}
\renewbibmacro{finentry}{\settoggle{lncs:lncs}{false}\finentry}
\DeclareBibliographyAlias{inbook}{inproceedings}
\DeclareFieldFormat[inbook]{title}{#1}
% END FIXES FOR BROKEN lncs.bbx

\renewcommand*{\postnotedelim}{}
\DeclareFieldFormat{postnote}{\mkbibparens{\mkpageprefix[pagination]    [\mknormrange]{#1}}}
\DeclareCiteCommand{\cite}
{\usebibmacro{prenote}%
	\bibopenbracket}
{\usebibmacro{citeindex}%
	\ifnumgreater{\value{citecount}}{1}
	{\bibopenbracket}
	{}%
	\usebibmacro{cite}}
{\bibclosebracket\multicitedelim}
{\bibclosebracket
	\usebibmacro{postnote}}

\addbibresource{zotero.bib}

\usepackage[autostyle=true]{csquotes} % Required to generate language-dependent quotes in the bibliography

\begin{document}
%
\title{Project Proposal: \protect\\Pixel-Stack Entropy Minimization \\for Joint Image Alignment}
%
%\titlerunning{Abbreviated Title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Jeffrey Spiers\inst{1}}
%
\authorrunning{J. Spiers}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{McGill University, Montreal, Canada\\
\email{jeffrey.spiers@mail.mcgill.ca}\\
Student ID 260122221}
%
\maketitle              % typeset the header of the contribution
%
%\begin{abstract}
%The abstract should briefly summarize the contents of the paper in
%150--250 words. JEFF
%
%\keywords{computer vision \and Markov random field \and texture modeling.}
%\end{abstract}
%
%
%
\section{Selected Paper}

I propose to implement the continuous joint alignment algorithm described in sections 2.1 to 2.4 of the 2006 PAMI paper of \citet{learned-miller_data_2006}.  The idea is to apply affine transformations to each image in a stack of images so as to minimize the sum, over all pixel positions, of the entropy of the pixel intensity values taken across the stack at each pixel position. The algorithm iteratively attempts to reduce the entropy by updating each one of a set of transformation parameters for each image until convergence is achieved. In order to avoid converging on a set of transforms that effectively destroys the input images -- for example, by scaling all the images down to zero size, thereby minimizing entropy by setting all pixel-intensities to 0 -- a penalty term is used to penalize large transformations.

\section{Testing}

\subsection{Dataset}
The \citet{learned-miller_data_2006} paper applied the entropy-minimization algorithm to the Special Database 19 of the National Institute of Standards and Technology (NIST).  I propose to test my implementation of the algorithm on the now popular MNIST\footnote{http://yann.lecun.com/exdb/mnist/} handwritten digits database derived therefrom.

If time allows and the algorithm is sufficiently efficient, I may consider aligning images from another dataset, such as a stack of face images.

\subsection{Binary vs Grayscale Pixel Intensities}
While the original NIST images had only binary \{0, 1\} pixel values, the MNIST database uses grayscale unsigned 8-bit pixel values in the range [0, 255].  For the sake of consistency and speed, I propose to initially convert the MNIST values to binary by means of simple binary thresholding.  If time allows and algorithmic performance is not too heavily impacted, I may also test the algorithm using the original, unthresholded MNIST grayscale values.

\subsection{Transformations}
\citet{learned-miller_data_2006} uses a set of 7 transformation parameters to represent all possible affine transformations.  The authors state that 6 parameters are technically sufficient to represent the complete set of affine transformations, but that the 7th may enable more rapid convergence by providing shortcuts through the transformation space. They give the example of rotation, which is equivalent to two shearing transformations and a rescaling, but more readily achieved via a separate rotation parameter.

For lack of computational resources, I plan to initially limit the range of allowable transformations to mere translations, perhaps expanding to rotation, scaling, and finally shearing.

\section{Classification}
In sections 2.6 and 2.7, \citet{learned-miller_data_2006} further describes how their algorithm can be used to classify test images by maximizing the posterior probability of the class given the new image data, which amounts to maximizing likelihood according to Bayes' rule as we have seen repeatedly.  In the unlikely event that there is time, I may also attempt to implement such a classifier.



%\begin{theorem}
%This is a sample theorem. The run-in heading is set in bold, while
%the following text appears in italics. Definitions, lemmas,
%propositions, and corollaries are styled the same way.
%\end{theorem}

% the environments 'definition', 'lemma', 'proposition', 'corollary',
% 'remark', and 'example' are defined in the LLNCS documentclass as well.
%

%\begin{proof}
%Proofs, examples, and remarks have the initial word in italics,
%while the following text appears in normal font.
%\end{proof}

%For citations of references, we prefer the use of square brackets
%and consecutive numbers. Citations using labels or the author/year
%convention are also acceptable. The following bibliography provides
%a sample reference list with entries for journal
%articles~\citep{}, an LNCS chapter~\cite{ref_lncs1}, a
%book~\cite{ref_book1}, proceedings without editors~\cite{ref_proc1},
%and a homepage~\cite{ref_url1}. Multiple citations are grouped
%\cite{ref_article1,ref_lncs1,ref_book1},
%\cite{ref_article1,ref_book1,ref_proc1,ref_url1}.

\nocite{}

%Here's a reference to Cross and Jain \citep{cross_markov_1983}.
%Here's a reference to multiple papers \citep{cross_markov_1983, fei-fei_bayesian_2005, leung_representing_2001}.
%
% ---- Bibliography ----

\printbibliography[title={References}] % Print the bibliography, section title in curly brackets

\end{document}
