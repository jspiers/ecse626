#!/usr/bin/env python3

# Standard Python
import sys
import os
import logging
import pickle
from glob import glob
from pprint import pprint

# Non-Standard Libraries
import numpy as np
import skimage
from skimage.io import imread, imsave
from skimage.filters import sobel

# Debug
# from pdb import set_trace
# import gnureadline as readline

# Local imports
from transform import Transform

# For reproducibility
# np.random.seed(0)


def mean_square_gradient(img):
    " Compute mean square gradient of the image, a measure of sharpness "
    gradient = sobel(img)
    return (gradient ** 2).mean()

def collect_stats(directory):
    " Build a dictionary of dictionaries from the -stats.pickle files in the given directory "
    assert os.path.isdir(directory)
    stats = {}
    suffix = "-stats.pickle"
    label_index = -len(suffix)-1
    for pickle_filename in sorted(glob(os.path.join(directory, "[0-9]{}".format(suffix)))):
        with open(pickle_filename, "rb") as f:
            stats[pickle_filename[label_index]] = pickle.load(f)
    return stats

def main(debug, directory):

    # Activate debug messages if debug flag was set on the command line
    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)

    assert os.path.isdir(directory)
    befores = []
    afters = []

    # Read transforms from pickle files
    print("Compiling pickled transforms...")
    transforms = {}
    for pickle_filename in sorted(glob(os.path.join(directory, "[0-9]-transforms.pickle"))):
        with open(pickle_filename, "rb") as f:
            transforms[pickle_filename[-len("-transforms.pickle")-1]] = pickle.load(f)
    pprint(transforms)

    print("Compiling pickled stats...")
    stats = collect_stats(directory)
    pprint(stats)

    print("Compiling before and after images...")
    for after_filename in sorted(glob(os.path.join(directory, "[0-9]-after.png"))):
        before_filename = after_filename[:-10] + "-before.png"
        assert os.path.isfile(before_filename), before_filename + " is not a file"
        print("\t{} => {}".format(before_filename, after_filename))
        befores.append(imread(before_filename))
        afters.append(imread(after_filename))

    # Write results image
    results_image = np.hstack([np.vstack(before_after) for before_after in zip(befores, afters)])
    image_filename = os.path.join(directory, "before-after.png")
    print("Writing {!r}".format(image_filename))
    imsave(image_filename, results_image)

    # Write average mean square gradients before and after
    print("Mean square gradients:")
    for i, before_after in enumerate(zip(befores, afters)):
        msgs = [mean_square_gradient(img) for img in before_after]
        print("\t{:7}: {:.4f} => {:.4f}".format(i, *msgs))
    msg_avg_before = np.mean([mean_square_gradient(img) for img in befores])
    msg_avg_after = np.mean([mean_square_gradient(img) for img in afters])
    print("\t-------------------------")
    print("\tOverall: {:.4f} => {:.4f}".format(msg_avg_before, msg_avg_after))

if __name__ == "__main__":
    import argparse
    print("Libraries:")
    print("\tPython v{}".format(sys.version.split(' ', 1)[0]))
    print("\tNumPy v{}".format(np.__version__))
    print("\tscikit-image v{}".format(skimage.__version__))

    ap = argparse.ArgumentParser("Report Tool")
    ap.add_argument("--debug", "-d", action="store_true")
    ap.add_argument("directory")
    sys.exit(main(**vars(ap.parse_args())))
