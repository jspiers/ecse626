MNIST Dataset
-------------
This "mnist" subdirectory should contain the gzipped MNIST handwritten digit database files (available from http://yann.lecun.com/exdb/mnist/)
