Dependencies
------------
- Python 3
- NumPy
- scikit-image
- python-mnist

Those are the main ones at least. In case of problems, also see "requirements.txt" for output of "pip freeze" in my Python environment.


MNIST Dataset
-------------
The "mnist" subdirectory must contain the gzipped MNIST handwritten digit database files (available from http://yann.lecun.com/exdb/mnist/)


Running the Alignment Algorithm
-------------------------------

"python3 align.py -h" provides command-line help instructions

Examples:
"python3 align.py" with no parameters will run a Gaussian update alignment strategy on all of the images for all of the digits from the MNIST test set.

To limit the number of images per digit (e.g. to the 200 images used in the examples in my report), set the -m switch with the maximum number of images:

"python3 align.py -m 200"

To run the conventional "vanilla" version of the algorithm which is more faithful to the Learned-Miller paper, use the -v flag (for "vanilla"):

"python3 align.py -m 200 -v"

Other flags include -s to enable mean subtraction, either after each iteration through the image stack:

"python3 align.py -m 200 -s every"

or only upon convergence when we would otherwise have quit, after which the cost function is updated and further attempts to reduce the cost are attempted once more:

"python3 align.py -m 200 -s end"

Operate in grayscale mode instead of binary (i.e. don't apply binary thresholding to the MNIST digits):

"python3 align.py -m 200 -g -v"

Modify the penalty term value of BETA:

"python3 align.py -m 200 -b 0.01"

Initially corrupt the input images by applying random transformations (with standard deviation as a parameter) before congealing. This was useful for testing but not used for the report.

"python3 align.py -m 200 -c 1"


Generating Before/After Comparison Images and Statistics
--------------------------------------------------------
The "align.py" script creates a subdirectory for its result files.
The "report.py" script can then be subsequently run with the directory name as a parameter to perform post-processing on the output data of the alignment script:

e.g. "python3 report.py gaussian-m200-bin-c0-never-b0_001"

The script will report the mean square gradient of the mean image before and after congealing for each digit, report the cost as a function of the number of transformations attempted, and concatenate the image files into a "before_after.png" file.


Generating Comparison Graphs
----------------------------
The graphs comparing the cost as a function of the number of attempted transformations presented in my report were generated using a Jupyter notebook:

"jupyter notebook notebook.ipynb"


