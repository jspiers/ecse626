#!/usr/bin/env python3

import numpy as np
import skimage
from skimage.io import imread, imsave
from skimage.util import img_as_ubyte
from skimage.transform import SimilarityTransform, AffineTransform, warp


class Transform:

    LABELS = ("sx", "sy", "r", "sh", "tx", "ty")

    IDENTITY = np.array([
        0.0, # log scale x
        0.0, # log scale y
        0.0, # rotation (radians)
        0.0, # shear (radians)
        0.0, # translate x
        0.0  # translate y
    ])

    DELTAS = np.array([
        0.01,           # log scale x
        0.01,           # log scale y
        0.01 * np.pi,   # rotation angle (radians)
        0.01 * np.pi,   # shear angle (radians)
        1.0,            # translate x
        1.0             # translate y
    ])

    def __init__(self, sx=0.0, sy=0.0, r=0.0, sh=0.0, tx=0.0, ty=0.0):
        self._params = np.array([sx, sy, r, sh, tx, ty])

    def params(self):
        return self._params

    def isIdentity(self):
        return (self._params == self.IDENTITY).all()

    def __str__(self):
        " Report all non-identity parameter values "
        return ' '.join("{}: {:6.4f}".format(k, v)
                        for k, v, i in zip(self.LABELS, self._params, self.IDENTITY) if v - i)

    __repr__ = __str__

    def __add__(self, other):
        return self.__class__(*self._params.__add__(other._params))

    def __sub__(self, other):
        return self.__class__(*self._params.__sub__(other._params))

    def __nonzero__(self):
        return not self.isIdentity()

    def penalty(self):
        change_steps = (self._params - self.IDENTITY) / self.DELTAS
        return (change_steps ** 2).sum()

#     def yieldMarginalGroups(self):
#         " Yield k, transforms with parameter k adjusted by +/- DELTAS[k] "
#         indices = np.arange(len(self.DELTAS))
#         plus = self._params + self.DELTAS
#         minus = self._params - self.DELTAS
#         for k, delta in enumerate(self.DELTAS):
#             params_plus_minus = (np.where(indices == k, plus_minus, self._params)
#                                  for plus_minus in (plus, minus))
#             yield (Transform(*p) for p in params_plus_minus)

    def yieldMarginals(self, k):
        " Yield transforms with parameter k adjusted by +/- DELTAS[k] "
        marginal = self._params.copy()
        marginal[k] += self.DELTAS[k]
        yield Transform(*marginal)
        marginal[k] -= 2 * self.DELTAS[k]
        yield Transform(*marginal)


    def marginalGaussian(self, std_dev):
        return self + self.gaussian(std_dev)

    def apply(self, image):
        assert image.dtype == np.uint8, image.dtype
        kwargs = {
            "scale" : np.exp(self._params[0:2]),
            "rotation": self._params[2],
            "shear": self._params[3],
            "translation": self._params[4:6]
        }
        affine_transform = AffineTransform(**kwargs)


        # Translate the center of the image to the origin before applying affine transform
        # (otherwise rotation is about the 0,0 corner of the image instead of the center)
        shift = np.array(image.shape) / 2 - 0.5
        center_to_origin = SimilarityTransform(translation=-shift)
        origin_to_center = SimilarityTransform(translation=shift)
        centered_affine_transform = center_to_origin + affine_transform + origin_to_center

        # Apply the transform and convert back to ubyte
        transformed_float = warp(image, centered_affine_transform.inverse)
        return img_as_ubyte(transformed_float)

    @classmethod
    def gaussian(cls, std_dev):
        """ Generate a transform by sampling a normal distribution
            with mean = identity transform and standard deviation = std_dev (argument)
        """
        # Verify assumption that 0.0 is the identity value for all parameters
        assert (cls.IDENTITY == 0.0).all()
        params = np.random.normal(0.0, std_dev, len(cls.DELTAS)) * cls.DELTAS
        return cls(*params)


    @classmethod
    def mean(cls, transforms):
        return cls(*np.vstack([t._params for t in transforms]).mean(axis=0))

    @classmethod
    def min(cls, transforms):
        return cls(*np.vstack([t._params for t in transforms]).min(axis=0))

    @classmethod
    def max(cls, transforms):
        return cls(*np.vstack([t._params for t in transforms]).max(axis=0))


def main(image_filename, sx, sy, r, sh, tx, ty):
    img = imread(image_filename)
    transform = Transform(sx, sy, r, sh, tx, ty)
    transformed = transform.apply(img)
    output_filename = "{}_transformed{}".format(*os.path.splitext(image_filename))
    imsave(output_filename, transformed)


if __name__ == "__main__":
    import sys
    import argparse
    import os

    print("Libraries:")
    print("\tPython v{}".format(sys.version.split(' ', 1)[0]))
    print("\tNumPy v{}".format(np.__version__))
    print("\tscikit-image v{}".format(skimage.__version__))

    ap = argparse.ArgumentParser("Transform Image")
    ap.add_argument("--sx", type=float, default=0.0, help="log(x-scale)")
    ap.add_argument("--sy", type=float, default=0.0, help="log(y-scale)")
    ap.add_argument("--r", "-r", type=float, default=0.0, help="rotation angle (rads)")
    ap.add_argument("--sh", type=float, default=0.0, help="shear angle (rads)")
    ap.add_argument("--tx", type=float, default=0.0, help="x-translation")
    ap.add_argument("--ty", type=float, default=0.0, help="y-translation")
    ap.add_argument("image_filename")
    sys.exit(main(**vars(ap.parse_args())))
