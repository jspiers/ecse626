% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

%\usepackage[
%doi=false
%,isbn=false
%,url=false
%,backend=biber
%,style=lncs
%]{biblatex}
\usepackage[backend=bibtex,style=lncs,natbib=true]{biblatex}


% FIXES FOR BROKEN lncs.bbx
\DeclareNameFormat{author}{%
	\ifdefvoid{\namepartprefix}{}{\namepartprefix\space}\namepartfamily, \namepartgiveni%
	\ifthenelse{\value{listcount}<\value{liststop}}
	{\addcomma\space}%
	{}%
}
\DeclareNameFormat{editor}{%
	\ifdefvoid{\namepartprefix}{}{\namepartprefix\space}\namepartfamily, \namepartgiveni%
	\ifthenelse{\value{listcount}<\value{liststop}}
	{\addcomma\space}%
	{\space\ifthenelse{\value{listcount}>1}
		{(\bibstring{editors})}
		{(\bibstring{editor})}}%
}
\renewcommand*{\finentrypunct}{}
\renewbibmacro{finentry}{\settoggle{lncs:lncs}{false}\finentry}
\DeclareBibliographyAlias{inbook}{inproceedings}
\DeclareFieldFormat[inbook]{title}{#1}
% END FIXES FOR BROKEN lncs.bbx

\renewcommand*{\postnotedelim}{}
\DeclareFieldFormat{postnote}{\mkbibparens{\mkpageprefix[pagination]    [\mknormrange]{#1}}}
\DeclareCiteCommand{\cite}
{\usebibmacro{prenote}%
	\bibopenbracket}
{\usebibmacro{citeindex}%
	\ifnumgreater{\value{citecount}}{1}
	{\bibopenbracket}
	{}%
	\usebibmacro{cite}}
{\bibclosebracket\multicitedelim}
{\bibclosebracket
	\usebibmacro{postnote}}

%\addbibresource{selected.bib}
\addbibresource{litreview.bib}

\usepackage[autostyle=true]{csquotes} % Required to generate language-dependent quotes in the bibliography

\begin{document}
%
\title{Statistical Texture Modeling: \protect\\ A Partial Survey}
%
%\titlerunning{Abbreviated Title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Jeffrey Spiers\inst{1}}
%
\authorrunning{J. Spiers}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{McGill University, Montreal, Canada\\
\email{jeffrey.spiers@mail.mcgill.ca}\\
Student ID 260122221}
%
\maketitle              % typeset the header of the contribution
%
%\begin{abstract}
%The abstract should briefly summarize the contents of the paper in
%150--250 words. JEFF
%
%\keywords{computer vision \and Markov random field \and texture modeling.}
%\end{abstract}
%
%
%
\section{Background}

\subsection{Defining Texture}
When presented with an image of a natural surface such as sand, slate, grass, or wood, a human observer can readily identify the depicted material by reference to various characteristic patterns of color and intensity in the image.  In the realm of photography and computer vision, the visual quality of these patterns is known as \textit{texture}, and though it is perceptually intuitive to humans, it is difficult to objectively formalize.  Most definitions in the computer vision literature describe texture as a homogeneous repetition of visual elements in an image, subject to a degree of randomization as to the pattern of repetition and the properties of the repeated elements \citep{li_markov_2009}.

\subsection{Relating Textural Perceptions to Statistical Measures}
Owing to its stochastic structure, image texture is well-suited to statistical analysis.  \citet{julesz_visual_1962} pioneered this field, formally describing textures as \(n^{th}\)-order joint empirical densities of image pixel values.  First-order statistics describe distributions over properties of pixels taken in isolation, such as the mean, variance, skew, and kurtosis of pixel intensity; second-order statistics provide more structural information by describing properties of pixel pairs; and higher order statistics describe yet more complex relationships among pixel groupings. In later work, he proposed that second-order statistics were sufficient to describe visually-perceptible textural features \citep{julesz_inability_1973}, but later still disproved his own conjecture by identifying a set of visually-distinct image pairs with identical second and third order statistics as counterexamples \citep{julesz_visual_1978}.

\section{Gray-Level Co-Occurence Models}
\subsection{Haralick et al. (1973)}
\citet{haralick_textural_1973} exploited second-order statistics about gray-level (pixel intensity) co-occurrence to perform texture classification.  Their method consisted of empirically measuring the frequency with which pairs of pixel intensity values co-occur within a spatially-defined local neighborhood of each pixel, then computing values for a set of 14 textural features derived from these co-occurrence statistics.  These values could then be used as a set of summary statistics for image texture. This in turn enabled textural classification of images by projecting images into the resulting 14-dimensional space and applying a discriminator trained on a set of images of known textures.

However, this approach is limited to the task of classification and does not provide a model capable of texture synthesis.

\section{Markov Random Field Models}

A Markov random field is a graphical probability model over a set of random variables, each represented as nodes in an undirected graph, with probabilistic dependencies between the variables represented as edges between their corresponding nodes.  Importantly, the edges in the graph represent the only dependencies among the random variables, such that any two random variables which do not share an edge may only influence each other via intervening variables along a path that connects them.  When every path between two random variables includes at least one random variable with a known value, the two random variables cannot influence one another and are therefore statistically independent.  
This is known as the Markov property.

\subsection{Cross and Jain (1983)}

Cross and Jain were among the first researchers to investigate the use of Markov random fields (MRFs) as texture models \citep{cross_markov_1983}. They model texture synthesis as a coloring of a two-dimensional lattice (represented as a Markov random field), where each node is a pixel and the "color" of the coloring is the intensity value assigned to the pixel.  Dependencies between each pixel and its neighbors are described by selecting a parametric model which defines the weighted influence of spatially-proximate pixels.  These are the "edges" of the graphical MRF representation.

They defined different orders of parametric texture models based on the membership criterion of nearby pixels in the neighborhood of a central pixel.  In a "first-order" model, only pixels immediately adjacent to a pixel in either the horizontal or vertical direction are considered to be in its neighborhood.  In second, third, and fourth order models, the neighborhood is extended to diagonally adjacent pixels, pixels two positions away in the horizontal and vertical directions, and pixels located at partial diagonals (i.e. where the total of x-displacement and y-displacement is 3), respectively.

Textures are defined by selecting the relative weights assigned to the edges connecting the central pixel to the various pixels in its neighborhood.  These weights serve to probabilistically influence the manner in which pixel values are updated.  Specifically, Cross and Jain model the conditional probability of a pixel value given the values of its neighbors according to an autobinomial distribution, which, following the result of \citet{besag_spatial_1974}, results in a conditional probability for the intensity value of pixel \(x_i\) given the values of its neighbors, \(P(x_i|neighbours(x_i))\), which is binomially distributed according to a parameter \(\theta\) given by: \[\theta(T) = \frac{e^T}{1 + e^T}\] where T is a weighted sum of neighboring pixel values weighted by the selected texture model parameters. The resulting texture is governed by the choice of model parameters, namely the relative weights assigned to each relative position in the neighborhood.  Cross and Jain provide examples of isotropic (directionally uniform) and anisotropic (directionally biased) textures synthesized using their approach.

\paragraph{Algorithm}
\begin{enumerate}
	\item Initialize the intensity value of each pixel at random by sampling from a uniform distribution of gray values.
	\item Iteratively loop over the following steps until convergence. This is a variant of the Metropolis-Hastings algorithm \citep{greenberg_understanding_1995}:
	\subitem Randomly select a fixed number of pairs of pixels.
	\subitem Compute the acceptance ratio \(\alpha = \frac{P(Y)}{P(X)}\), where \(P(X)\) is the joint probability of the current assignment, \(X\), of pixel intensity values over the entire image, and \(P(Y)\) is the joint probability of a new assignment, \(Y\), which is identical to \(X\) except that the intensity values of the randomly selected pairs of pixels are swapped.
	\subitem If \(\alpha \ge 1\), accept the pixel value swap. Else accept the pixel value swap with probability \(\alpha\).
\end{enumerate}

\noindent \citet{hammersley_monte_2013} proved that this algorithm converges.

\begin{figure}
\begin{center}
	\includegraphics[width=0.93\textwidth]{textures.png}
	\caption{Textures from the CUReT database \citep{leung_representing_2001} \label{fig1}}
\end{center}
\end{figure}

\subsection{Derin and Elliott (1987) }

\citet{derin_modeling_1987} extended the result of \citet{cross_markov_1983} by using a different variety of auto-model, namely a multilevel logistic model instead of the autobinomial model.  The advantage of a multilevel model is that unlike a simple logistic model (or the autobinomial model employed by \citet{cross_markov_1983}) that includes weights on a pairwise basis (i.e. with cliques of 2 pixels), a multilevel model may further incorporate weights for cliques of size 3 or greater, enabling modeling of probability distributions that depend on relationships among groups of multiple pixels.

\section{Texton-Based Models}

\subsection{Leung and Malik (2001) }

In their highly-cited paper, \citet{leung_representing_2001} identified a finite set of texture primitives, called "textons", which can be combined in a linear fashion to synthesize artificial textures that convincingly mimic the appearance of naturally-occurring textures such as concrete, rug, marble, and leather.

They did this by clustering the filter responses of 48 filters applied to a set of texture images from the Columbia-Utrecht (CUReT) database (see Figure \ref{fig1}).

Similar to principal component analysis (PCA), their approach is a form of dimensionality reduction, but specific to the task of identifying a set of basis functions for modeling texture.

\subsection{Varma and Zisserman (2003) }

\citet{varma_texture_2003} combined the texton-based technique of \citet{leung_representing_2001} and MRFs to develop a novel texture classification strategy.

Their solution operates in two phases.  In a training phase, a library of textons is learned by clustering the filter responses of 10 types on a set of training images in the manner of \citet{leung_representing_2001}.  This acts as a form of dimensionality reduction of their training set, projecting each image in the set into an n-dimensional space, where n is the number of identified textons across the set of images.  Their training set consisted of 2806 images, which were mapped to 610 textons.  The weighted response for each texture image was computed by labelling each pixel in the training image with the texton that lied closest to it in the filter response space. A histogram of texton pixel-labels was then computed as the weighted response of that texture image to the texton filter bank.

In a pixel-wise version of the classification algorithm, the classification stage consisted of repeating the steps performed on the training image and projecting the image into texton-space according to a histogram of pixel-wise texton correspondences. But \citet{varma_texture_2003} also developed a more effective version of the classification algorithm that performed classification not on the basis of each pixel's most closely associated texton response, but on the texton response of that pixel as well as that of its neighbours.  The classification performance of this windowed Markov random field version of their algorithm exceeded the performance of the pixel-wise classification algorithm.  A key contribution of this paper was therefore to demonstrate that a pixel-wise approach to a classification task can be improved by incorporating a Markov random fields among pixels to allow influence from the classifications estimated for neighboring pixels. In other words, the spatial constraints of MRFs can be used to improve classification.


%\begin{theorem}
%This is a sample theorem. The run-in heading is set in bold, while
%the following text appears in italics. Definitions, lemmas,
%propositions, and corollaries are styled the same way.
%\end{theorem}

% the environments 'definition', 'lemma', 'proposition', 'corollary',
% 'remark', and 'example' are defined in the LLNCS documentclass as well.
%

%\begin{proof}
%Proofs, examples, and remarks have the initial word in italics,
%while the following text appears in normal font.
%\end{proof}

%For citations of references, we prefer the use of square brackets
%and consecutive numbers. Citations using labels or the author/year
%convention are also acceptable. The following bibliography provides
%a sample reference list with entries for journal
%articles~\citep{}, an LNCS chapter~\cite{ref_lncs1}, a
%book~\cite{ref_book1}, proceedings without editors~\cite{ref_proc1},
%and a homepage~\cite{ref_url1}. Multiple citations are grouped
%\cite{ref_article1,ref_lncs1,ref_book1},
%\cite{ref_article1,ref_book1,ref_proc1,ref_url1}.

\nocite{}

%Here's a reference to Cross and Jain \citep{cross_markov_1983}.
%Here's a reference to multiple papers \citep{cross_markov_1983, fei-fei_bayesian_2005, leung_representing_2001}.
%
% ---- Bibliography ----

\printbibliography[title={References}] % Print the bibliography, section title in curly brackets

\end{document}
