#!/usr/bin/env python3

# Standard python imports
import sys
import os
import warnings
import argparse
from glob import glob
from itertools import count

# Library imports
# import cv2 as cv
import numpy as np
import skimage
from skimage.segmentation import slic
from skimage.util import img_as_float
from skimage.io import imread
import scipy

# Local imports
from utils import imsave_ignore
from em import GMM
import npjson

# For reproducible results
np.random.seed(0)

# Image filenames
IMAGE_FILENAME = "flower.jpg"
MASK_FILENAME = "flower-anno.png"

# Color definitions for scribble mask
COLORS_RGB = {
    #           R    G    B
    "black"  : (0,   0,   0  ),
    "yellow" : (255, 255, 207),
    "red"    : (219, 0,   0  )
}
RGB_COLORS = {v: k for k, v in COLORS_RGB.items()}
FG_COLOR = COLORS_RGB["yellow"]
BG_COLOR = COLORS_RGB["red"]




def fit_gaussian(colors):
    " Instantiate a normal distribution model from the sample mean and covariance "
    mean = np.mean(colors, axis=0)
    cov = np.cov(colors, rowvar=False)
    return scipy.stats.multivariate_normal(mean=mean, cov=cov)


def write_masked(filename, img, mask, default_color=(0, 0, 0)):
    " Write only the masked part of the image to file "
    masked_img = np.where(mask[..., None], img, default_color)
    imsave_ignore(filename, masked_img)


def write_patches(filename, colors, shape=(10, 100)):
    " Write an image to file with patches of each one of the colors "
    patches = [np.full(shape + (3,), color) for color in colors]
    imsave_ignore(filename, np.vstack(patches))


def diff_matrix(F):
    " Return matrix of pairwise differences between elements of F "
    return (F[np.newaxis, :] != F[:, np.newaxis]).reshape((F.shape[0], F.shape[0]))


def gmm_label(num_labels, train_data, predict_data, hint_color_label=None):
    " Initialize labels as those assigned from a GMM using EM "
    gmm = GMM(num_labels)
    gmm.em(train_data, verbosity=0)
    labels = gmm.predict(predict_data)

    # Shift labels if necessary to make sure hint_color gets the correct hint_label
    if hint_color_label is not None:
        color, label = hint_color_label
        label_diff = gmm.predict(color) - label
        if label_diff.any():
            # make sure shift is positive to avoid negative numbers
            labels = (labels + label_diff + num_labels) % num_labels

    return labels


class BayesianMRF:
    " Bayesian Markov Random Field for Superpixel Segmentation "

    def __init__(self, img):
        self._img = img_as_float(img)
        self._spix = None
        self._clear_spix_dependencies()
        self._beta = None
        self._log_like_model = None
        self._log_like_select = None

    def _clear_spix_dependencies(self):
        " Clear any computed variables that depend on self._spix "
        self._S = None
        self._S_colors = None
        self._E = None
        self._A = None

    def image(self):
        " Get image "
        return self._img

    def superpixelate(self, n_pixels_hint):
        " Generate superpixel image using SLIC algorithm "
        self._spix = slic(self._img, n_pixels_hint)
        self._clear_spix_dependencies()

        # Return number of superpixels
        return self._spix.max() + 1

    def superpixel_image(self):
        """ Color each superpixel with the average color of
            its member pixels in the original image
        """
        assert self._spix is not None
        if self._S is None:
            self._S = np.zeros(self._img.shape)
            for p in np.unique(self._spix):
                mask = self._spix == p
                self._S[mask] = self._img[mask].mean(axis=0)
        return self._S

    def superpixel_colors(self):
        " Return all unique superpixel colors "
        if self._S_colors is None:
            self._S_colors = np.unique(self._S.reshape(-1, self._S.shape[-1]), axis=0)

            # Make sure we weren't unlucky enough to have two superpixels with exactly
            # the same color, which would screw up our assumption that
            # indexes for S_colors, F have a 1-to-1 correspondence
            assert self._S_colors.shape[0] == self._spix.max() + 1

        return self._S_colors


    def superpixel_adjacency_pairs(self):
        " Return list E of all adjacent pairs (i,j) of neighboring superpixels "
        if self._E is None:
            # Make sure the superpixels have contiguous values in range 0..spix.max()
            assert np.bincount(self._spix.flatten()).all()

            # Find vertical and horizontal neighbors by stacking shifted versions of superpixel matrix
            row_neighs = np.unique(np.dstack((self._spix[:-1,:], self._spix[1:,:])), axis=1)
            col_neighs = np.unique(np.dstack((self._spix[:,:-1], self._spix[:,1:])), axis=0)
            all_neighs = np.concatenate((row_neighs.reshape(-1,2), col_neighs.reshape(-1,2)))

            # Filter out repeated pairs or identity pairs (i.e. must be different superpixels)
            self._E = np.array([p for p in np.unique(np.sort(all_neighs), axis=0) if p[0] != p[1]])

        return self._E


    def superpixel_adjacency_matrix(self):
        " Build bidirectional adjacency matrix A from list E of (i, j) adjacency pairs "
        if self._A is None:
            # Initialize the adjacency matrix to all False (0)
            dim = self._spix.max() + 1
            self._A = np.zeros((dim, dim), dtype=np.int)

            # Set adjacency of neighbors to True (1)
            for pair in self.superpixel_adjacency_pairs():
                self._A[pair[0], pair[1]] = 1 #True
                self._A[pair[1], pair[0]] = 1 #True

        return self._A

    def foreground_image(self, F):
        " Represent the segmented image by masking out background colors (i.e. where F==0) "
        img = self._S.copy()
        for bg_color in self._S_colors[F == 0]:
            img[self._S==bg_color] = 0
        return img

    def loglikelihood(self, F):
        " Sum of log likelihoods selected from logP(S|F=BG) or logP(S|F=FG) according to labels F "
        return self._log_like_model[self._log_like_select, F].sum()

    def adjacent_disagreement_count(self, F):
        " Compute number of adjacent superpixels with different labels "
        return self._A[diff_matrix(F)].sum()

    def logprior(self, F, beta):
        " Equal to adjacent disagreements weighted by beta "
        return -beta * self.adjacent_disagreement_count(F)

    def ground_truth(self, examples_per_label):
        " Fit Gaussians to example colors for each label (i.e. S_bg, S_fg) "
        gaussians = [fit_gaussian(examples) for examples in examples_per_label]
        self._compute_log_likelihoods(gaussians)
        return gaussians

    def _compute_log_likelihoods(self, gaussians):
        " Compute log likelihood according to per-label Gaussians of each color "
        log_likes_label_by_color = [g.logpdf(self._S_colors) for g in gaussians]
        self._log_like_model = np.vstack(log_likes_label_by_color).T
        self._log_like_select = np.arange(len(self._log_like_model))

    @classmethod
    def variance(cls, t):
        # variance as a function of t as defined in question 3.3.2
        return 3.0 / np.log(1 + t)

    @classmethod
    def label_change_probability(cls, t):
        """ Probability of a label change from 0=>1 or 1=>0 at time t,
            computed based on the probability of sampling a value that deviates by 1
            from the mean of a Gaussian having variance sigma^2=var_t(t)
            relative to the probability of sampling a value equal to its mean
        """
        p_same = 1 / (1 + np.exp(-1 / (2 * cls.variance(t))))
        return 1 - p_same

    def proposal(self, F, t):
        " Flip each of the values {0, 1} of F with probability 'label_change_probability(t)' "
        change_mask = np.random.uniform(size=F.shape[0]) < self.label_change_probability(t)
        return np.where(change_mask, F ^ 1, F)


    def map_metropolis(self, F0, beta, MAX_HIT_SAME_COUNT=50):
        """ Apply the Metropolis update algorithm, beginning at F = F0,
            terminating when the same label assignment is proposed MAX_HIT_SAME_COUNT times
        """
        beta_str = str(beta).replace('.', '_')

        # Delete JPGs from previous runs
        F_img_fmt = "S{}-beta{}-F{{}}.jpg".format(self._spix.max()+1, beta_str)
        t_width = 10
        print("Writing progressive segmentations to {!r}".format(F_img_fmt.format('<t>')))
        for filename in glob(F_img_fmt.format('*')):
            os.remove(filename)

        # Initialize F using F0 previously determined by GMM(k=2) color model
        F = F0

        # Define segmentation image filename format and dump image for F0
        F_img = self.foreground_image(F)
        imsave_ignore(F_img_fmt.format(str(0).zfill(t_width)), np.hstack((self._S, F_img)))

        print("beta = {}".format(beta))
        print("   t    | log P(S,F)  | log P(S|F) |  log P(F)  ")
        print("-------------------------------------------------")
        fmt = "{:<7} |  {:8.1f} {} |  {:8.1f}  | {:8.1f}"

        # t = 0
        # Compute probabilities for initial classification
        log_like = self.loglikelihood(F)
        log_prior = self.logprior(F, beta)
        log_joint = log_like + log_prior

        # Display the timestep, log P(S,F), log P(S|F), and log P(F) for time 0
        print(fmt.format(0, log_joint, ' ', log_like, log_prior))

        hit_same_count = 0
        best_F = F0
        best_log_joint = log_joint
        for t in count(1):
            F_new = self.proposal(F, t)

            # Check if the new label assignment is unchanged
            if (F_new == F).all():
                hit_same_count += 1
                q = self.label_change_probability(t)
                print("{:<7} F_new == F (count={:03}) q(ft|ft-1)={:g}".format(t, hit_same_count, q))
                continue

            # Compute probabilities for proposed classification F_new
            log_like_new = self.loglikelihood(F_new)
            log_prior_new = self.logprior(F_new, beta)
            log_joint_new = log_like_new + log_prior_new

            # Compute log improvement of joint probability relative to that of current F
            log_improvement = log_joint_new - log_joint

            # Metropolis update
            # The following resolves True with p = min(1, p_joint_new / p_joint)
            if np.log(np.random.uniform()) < log_improvement:

                # Display the timestep, log P(S,F), log P(S|F), and log P(F) for the new F
                # Use a '*' to indicate cases where p_joint_new < p_joint
                # but F_new was selected anyways according to Metropolis update
                metropolis = '*' if log_improvement < 0 else ' '
                print(fmt.format(t, log_joint_new, metropolis, log_like_new, log_prior_new))

                # Update F and the log P(S,F) joint probability
                F = F_new
                log_joint = log_joint_new

                # Update best F encountered so far
                if log_joint > best_log_joint:
                    best_F = F
                    best_log_joint = log_joint

                # Write an image representing the FG/BG classification F
                F_img = self.foreground_image(F)
                imsave_ignore(F_img_fmt.format(str(t).zfill(t_width)), np.hstack((self._S, F_img)))

            # Termination condition:
            #   When the proposed F (F_new) has been unchanged at least MAX_HIT_SAME_COUNT times,
            #   the search is starting to lose steam
            if hit_same_count >= MAX_HIT_SAME_COUNT:
                print("This seems to be the best we're gonna do")
                break

        # Write an image representing the FG/BG classification F
        F_img = self.foreground_image(F)
        imsave_ignore(F_img_fmt.format("inal"), np.hstack((self._S, F_img)))

        return best_F


def extract_masked_colors(S, mask_img, mask_colors):
    " Extract background and foreground colors from image S using scribble mask "
    for color in mask_colors:
        # https://stackoverflow.com/a/44243100
        mask = (mask_img == color).all(-1)
        yield np.unique(S[mask], axis=0)



def main(image_filename, mask_filename, sanity_check=False):

    # Load image
    img = img_as_float(imread(image_filename))
    print("Loaded {!r} with shape {}".format(image_filename, img.shape))

    # Initialize Bayesian MRF model for the image
    mrf = BayesianMRF(img)

    # Generate superpixel image
    S_num = mrf.superpixelate(180)

    # Color the superpixels with the average color of the superpixel region in img
    S = mrf.superpixel_image()

    # Write the superpixel image to file
    fn = "S{}.jpg".format(S_num)
    imsave_ignore(fn, np.hstack((img, S)))
    print("{} superpixels saved to {!r}".format(S_num, fn))

    # Get all unique superpixel colors
    S_colors = mrf.superpixel_colors()

    # Get the superpixel adjacency matrix and dump it to file
    A = mrf.superpixel_adjacency_matrix()
    npjson.dump("A.json", A)

    # Extract FG and BG colors from the superpixel image using scribble mask
    mask_img = imread(mask_filename)
    if sanity_check:
        print("Extracting scribble mask colors from {!r}:".format(mask_filename))
        for rgb in np.unique(mask_img.reshape(-1, mask_img.shape[-1]), axis=0):
            print("\t{} {}".format(tuple(rgb), RGB_COLORS.get(tuple(rgb), "unknown")))
    S_bg, S_fg = tuple(extract_masked_colors(S, mask_img, (BG_COLOR, FG_COLOR)))

    # Write patch images with ground truth FG and BG colors (sanity check)
    for label, colors in (("All", S_colors), ("FG", S_fg), ("BG", S_bg)):
        fn = "S{}_{}.png".format(S_num, label.lower())
        write_patches(fn, colors)
        print("{} colors written to {!r}".format(label, fn))

    # Set the ground truth colors and fit Gaussians for each of BG, FG
    gaussians = mrf.ground_truth((S_bg, S_fg))

    # Dump the mean and covariance of the fitted N_bg and N_fg Gaussians to file
    for label, gaussian in enumerate(gaussians):
        label_str = {0: "bg", 1: "fg"}[label]
        filename = "N{}_{}.json".format(S_num, label_str)
        npjson.dump(filename, {"mean": gaussian.mean, "cov": gaussian.cov})
        print("P(s|f={}) Gaussian dumped to {!r}".format(label_str, filename))

    # Initialize the label assignment using a GMM of 2 Gaussians
    # Use yellow as a foreground color hint
    yellow = np.array([[172, 141, 35]]) / 255
    hint_color_label = (yellow, 1) # FG == 1
    F0 = gmm_label(2, img.reshape(-1, img.shape[-1]), S_colors, hint_color_label)

    # Apply the Metropolis update algorithm, beginning at F = F0, for three different
    # values of beta (adjacency coherence weight)
    for beta in (0.1, 2, 40):
        mrf.map_metropolis(F0, beta)


if __name__ == '__main__':
    print("Libraries:")
    print("\tPython v{}".format(sys.version.split(' ', 1)[0]))
    print("\tNumPy v{}".format(np.__version__))
    print("\tSciPy v{}".format(scipy.__version__))
    print("\tscikit-image v{}".format(skimage.__version__))
    ap = argparse.ArgumentParser("Bayesian MRF")
    ap.add_argument("image_filename")
    ap.add_argument("mask_filename")
    sys.exit(main(**vars(ap.parse_args())))
