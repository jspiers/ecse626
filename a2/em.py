#!/usr/bin/env python3

# Standard Python
import sys
import json
from collections import defaultdict
from pprint import pprint

# Libraries
import skimage
from skimage.io import imread, imsave
import numpy as np
import scipy
from scipy.stats import multivariate_normal
from scipy.special import logsumexp

IMAGE_FILENAME = "flower.jpg"
BICS_FILENAME = "bics.json"

# For reproducible results
np.random.seed(0)

class GMM:
    " Gaussian Mixture Model "

    def __init__(self, k):
        " Initialize GMM with 'k' Gaussian components "
        super(GMM, self).__init__()
        self._k = k
        self._weights = None
        self._means = None
        self._covs = None

    def num_components(self):
        " Get number of Gaussian components "
        return self._k

    def weights(self, i=None):
        " Get weight of Gaussian 'i' or all "
        if self._weights is None:
            return None
        if i is None:
            return self._weights.copy()
        return self._weights[i]

    def means(self, i=None):
        " Get mean of Gaussian 'i' or all "
        if self._means is None:
            return None
        if i is None:
            return self._means.copy()
        return self._means[i]

    def covariances(self, i):
        " Get covariance matrix of Gaussian 'i' or all "
        if self._covs is None:
            return None
        if i is None:
            return self._covs.copy()
        return self._covs[i]

    def _mstep(self, data, labels):
        " M-step of the EM algorithm: compute best GMM parameters based on the labels/data "
        self._weights = []
        self._means = []
        self._covs = []
        for i in range(self._k):
            data_i = data[labels==i]
            self._weights.append(data_i.shape[0] / data.shape[0])
            self._means.append(np.mean(data_i, axis=0))
            self._covs.append(np.cov(data_i, rowvar=False))
#         print("weights", self._weights)
#         print("means", self._means)
#         print("covs", self._covs)

    def _log_probs(self, data):
        log_probs = [multivariate_normal.logpdf(data, mean=mean, cov=cov)
                     for mean, cov in zip(self._means, self._covs)]
        return np.vstack(log_probs).T

    def _log_probs_weighted(self, data):
        # Compute log probabilities of the data for each Gaussian component
        log_probs = self._log_probs(data)

        # Compute log of weights of the Gaussians
        log_weights = np.log(self._weights)

        # Weight the log probabilities by the log weights
        log_probs_weighted = log_probs + log_weights
        return log_probs_weighted

    def _estep(self, data):
        """ E-step of the EM algorithm: label data points with index of
            the Gaussian component contributing most to its value
        """
        # Get weighted log probabilities
        log_probs_weighted = self._log_probs_weighted(data)

        # Compute the log of the sum of exp of the weighted log probs
        # as the normalization factor to figure out relative contribution of
        # each Gaussian
        normalization_factor = logsumexp(log_probs_weighted, axis=1)

        # Compute the estimated contribution of each Gaussian to the data values
        log_contributions = log_probs_weighted - normalization_factor[:, np.newaxis]

        # Label each data point with the index of the Gaussian contributing most to its value
        labels = log_contributions.argmax(axis=1)
        return labels

    def predict(self, data):
        " Classify the elements of data using current "
        assert self._weights is not None
        return self._estep(data)

    def score_samples(self, data):
        # Get weighted log probabilities
        log_probs_weighted = self._log_probs_weighted(data)
        return logsumexp(log_probs_weighted, axis=1)

    def bic(self, data):
        " Compute the Bayesian Information Criterion for this data sample and GMM parameters "
        return -2 * self.score_samples(data).sum() + self._k * np.log(data.shape[0])

    def _init_labels(self, data, init="semirandom"):
        " Initialize the label assignment semi-randomly "
        assert init == "semirandom", "Only random initialization has been implemented"

        # Generate a vector of random labels 1/4 the length of the data
        # Sort the vector and concatenate it with itself
        # The result is to assign the same label to long strings of adjacent pixels
        # which should provide a better than random initial assignment
        # since adjacent pixels in natural images tend to have similar colors
        rand = np.sort(np.random.randint(self._k, size=(data.shape[0]+3)//4))
        labels = np.hstack((rand,) * 4)[:data.shape[0]]
        return labels

    def em(self, data, init="semirandom", verbosity=0):
        " Fit the GMM model to the given data iteratively using the EM algorithm "

        assert len(data.shape) == 2

        # Initialize with semi-random labels
        labels = self._init_labels(data, init)

        while True:
            # Report progress
            if verbosity > 1:
                print("label counts", np.bincount(labels))
            elif verbosity == 1:
                print('.', end='')
                sys.stdout.flush()

            # Make sure there aren't any unused labels (because it breaks the M step)
            if not np.bincount(labels, minlength=self._k).all():
                print("got a 0 label count :( ... reinitializing")
                labels = self._init_labels(data, init)

            # M-step: compute weights, means, covariances of Gaussians implied by the labels
            self._mstep(data, labels)

            # E-step: determine new labels using the new Gaussian parameters
            labels_new = self._estep(data)

            # Upon convergence, return the labels
            if (labels_new == labels).all():
                return labels

            # Else update and iterate
            labels = labels_new


def main(image_filename, write_bics=True):

    # Load image
    img = imread(image_filename)
    print("Loaded {!r} with shape {}".format(image_filename, img.shape))

    # Reshape 2D image as a vector
    img_vector = img.reshape(-1, img.shape[-1])

    # Initialize dictionary of BIC values to be saved to JSON (to produce graph for report)
    bics = defaultdict(list)

    # Fit GMMs with 2..8 components
    attempts_per_m = 5
    for m in range(2, 9):
        print("Fitting GMM with m = {} components...".format(m))
        for attempt in range(1, attempts_per_m + 1):
            print("\tattempt {}:".format(attempt), end=' ')
            sys.stdout.flush()

            # Fit the GMM via EM and make label predictions
            gmm = GMM(m)
            print('')
            labels = gmm.em(img_vector, verbosity=2)

            # Use the mean (color) of my GMM components to represent the segmentation labels
            labels_img = np.array([gmm.means(i) for i in labels], dtype=np.uint8).reshape(img.shape)
            labels_filename = "labels-m{}-{}.jpg".format(m, attempt)
            imsave(labels_filename, labels_img)

            # Get log probabilities and compute BIC
            bic = gmm.bic(img_vector)
            print("BIC = {}".format(bic))

            # Accumulate BICs to be able to plot them later
            bics[m].append(bic)

    # Display BICs
    print("BICs Summary:")
    pprint(bics)

    # Write BICs to file for later processing
    if write_bics:
        print("Writing BICs to {!r}".format(BICS_FILENAME))
        with open(BICS_FILENAME, 'w') as f:
            json.dump(bics, f, sort_keys=True, indent=2)


if __name__ == '__main__':
    print("Libraries:")
    print("\tPython v{}".format(sys.version.split(' ', 1)[0]))
    print("\tscikit-image v{}".format(skimage.__version__))
    print("\tNumPy v{}".format(np.__version__))
    print("\tSciPy v{}".format(scipy.__version__))
    sys.exit(main(IMAGE_FILENAME))
