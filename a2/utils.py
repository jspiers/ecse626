import warnings
from skimage.io import imsave

def imsave_ignore(*args, **kwargs):
    " skimage.io.imsave with annoying warnings suppressed "
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        imsave(*args, **kwargs)
