import json
import numpy as np

# https://stackoverflow.com/a/47626762
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def dump(filename, ndarray_):
    with open(filename, 'w') as f:
        json.dump(ndarray_, f, cls=NumpyEncoder)

def load(filename):
    with open(filename, 'r') as f:
        return np.asarray(json.load(f))
