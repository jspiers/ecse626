#!/usr/bin/env sh
set -x
python3 -V
python3 -c 'import cv2, numpy;print("NumPy v" + numpy.__version__);print("OpenCV v" + cv2.__version__)'
python3 -m unittest discover -v .
